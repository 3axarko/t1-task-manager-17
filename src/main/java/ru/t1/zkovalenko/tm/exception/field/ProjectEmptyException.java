package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class ProjectEmptyException extends AbstractException {

    public ProjectEmptyException() {
        super("Project is empty");
    }

}
