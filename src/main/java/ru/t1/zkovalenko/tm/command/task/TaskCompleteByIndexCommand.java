package ru.t1.zkovalenko.tm.command.task;

import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-complete-by-index";

    public static final String DESCRIPTION = "Task complete by index";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
