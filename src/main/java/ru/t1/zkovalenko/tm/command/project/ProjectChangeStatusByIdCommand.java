package ru.t1.zkovalenko.tm.command.project;

import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-change-status-by-id";

    public static final String DESCRIPTION = "Project change status by id";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("AVAILABLE STATUSES:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.println("ENTER STATUS: ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusById(id, status);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
