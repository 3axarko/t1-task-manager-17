package ru.t1.zkovalenko.tm.command.system;

import ru.t1.zkovalenko.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandListCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";

    public static final String ARGUMENT = "-cmd";

    public static final String DESCRIPTION = "List of commands";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
